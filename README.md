# O que é Terraform

Terraform permite criar, alterar e melhorar a infraestrutura de forma segura e previsível. É uma ferramenta de software livre que codifica as APIs em arquivos de configuração declarativa que podem ser compartilhados entre os membros da equipe, tratados como código, editados, revisados e versionados. Para maiores informações [Clique aqui](https://www.terraform.io/ "Terraform"), para realizar o download e instalação [clique aqui](https://www.terraform.io/downloads.html "Download"), e escolha o seu sistema operacional de sua preferencia. Para consultar a documentação ofical [Clique aqui](https://www.terraform.io/docs/ "Documento Oficial")

## Práticas Recomendadas

A infraestrutura colaborativa como fluxo de trabalho de código é baseada em muitas outras práticas recomendadas de TI (como o uso de controle de versão e a prevenção de alterações manuais) e você deve adotar essas bases antes de adotar totalmente o fluxo de trabalho recomendado. A obtenção de práticas de provisionamento de ponta é uma jornada, com várias paradas distintas ao longo do caminho.

Este [guia](https://www.terraform.io/docs/cloud/guides/recommended-practices/index.html "Práticas Recomendadas") descreve as práticas recomendadas do Terraform e como adotá-las. Abrange os passos para começar a usar nossas ferramentas, com atenção especial para as práticas fundamentais em que elas confiam.

## Principais Comandos

Este é um [guia](https://learn.hashicorp.com/terraform/development/running-terraform-in-automation " Running Terraform in Automation") avançado! Ao começar a usar o Terraform, é recomendado usá-lo localmente a partir da linha de comando. A automação pode se tornar valiosa quando o Terraform estiver sendo usado regularmente na produção ou por uma equipe maior, mas este guia pressupõe familiaridade com o fluxo de trabalho CLI local normal. A baixo segue os comandos mais utilizados, favor ler o guia para maiores informações.

1. `terraform init -input=false:` Inicializar o diretório de trabalho

1. `terraform plan -out=tfplan -input=false:` Cria um plano e salva o tfplan arquivo local
   - A opção `-input=false` indica que o Terraform não deve tentar solicitar a entrada e esperar que todos os valores necessários sejam fornecidos pelos arquivos de configuração ou pela linha de comando. Portanto, pode ser necessário usar as opções `-var` e `-var-file` no plano terraform para especificar quaisquer valores de variáveis que tradicionalmente teriam sido inseridos manualmente sob uso interativo.

1. `terraform apply -input=false tfplan:` Para aplicar o plano armazenado no arquivo tfplan
   - É altamente recomendável usar um backend que suporte estado remoto, desde que isso permita que o Terraform salve automaticamente o estado em um local persistente onde ele possa ser encontrado e atualizado por execuções subseqüentes. A seleção de um back-end que suporte o bloqueio de estado também oferece segurança contra condições de corrida que podem ser causadas por execuções simultâneas do Terraform.

1. `terraform apply -input=false -auto-approve:` A opção -auto-approve diz ao Terraform para não exigir aprovação interativa do plano antes de aplicá-lo.

- `terraform plan -destroy:` Mostra o que vai ser removido
- `terraform destroy:` Destroi infra criada anteriomente
- `terraform destroy -auto-approve:` A opção -auto-approve diz ao Terraform para não exigir aprovação interativa do plano antes de destrui-lo.
